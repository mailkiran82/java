package org.mailkiran82.springboot.featureflagdemo.basic;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@ConfigurationProperties("feature")
public class FeatureConfiguration {
    private Map<String, Boolean> toggles = new HashMap<>();

    public Map<String, Boolean> getToggles() {
        return toggles;
    }

    public boolean videoEnabled() {
        return toggles.getOrDefault("videos", false);
    }
}
