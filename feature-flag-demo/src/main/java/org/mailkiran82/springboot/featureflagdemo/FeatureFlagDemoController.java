package org.mailkiran82.springboot.featureflagdemo;

import org.mailkiran82.springboot.featureflagdemo.basic.FeatureConfiguration;
import org.mailkiran82.springboot.featureflagdemo.interceptor.FeatureRepository;
import org.mailkiran82.springboot.featureflagdemo.interceptor.FeatureToggle;
import org.mailkiran82.springboot.featureflagdemo.unleash.UnleashFeatureFlags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeatureFlagDemoController {

    //basic
    @Autowired
    private FeatureConfiguration featureConfiguration;

    //interceptor
    @Autowired
    private FeatureRepository featureRepository;

    //unleash
    @Autowired
    private UnleashFeatureFlags unleashFeatureFlags;

    @GetMapping("/file/books")
    @ResponseBody
    public String getBooks(){
        return "Welcome to Books section.";
    }

    //basic
    @GetMapping("/file/videos")
    @ResponseBody
    public String getVideos(){
        if (!featureConfiguration.videoEnabled()) {
            return "Videos section is under construction";
        }

        return "Welcome to Videos section.";
    }

    //interceptor
    @GetMapping("/file/news")
    @ResponseBody
    @FeatureToggle(feature = "feature.toggles.news", expectedToBeOn = true)
    public String getNews(){
        return "Welcome to News section.";
    }

    //unleash
    @GetMapping("/unleash/blogs")
    @ResponseBody
    public String getBlogs(@RequestParam String id){
        return  "Blogs section" + "<br>"
                + "On/Off Strategy: "+unleashFeatureFlags.isToggleActive("ON_OFF") + "<br>"
                + "By User Strategy: "+unleashFeatureFlags.isToggleActiveByUser("BY_USER", id) + "<br>"
                //+ "Flexible Rollout Strategy: "+unleashFeatureFlags.isToggleActive("FLEXIBLE_ROLLOUT") + "<br>"
                + "Flexible Rollout By User Strategy: "+unleashFeatureFlags.isToggleActiveByUser("FLEXIBLE_ROLLOUT" , id) + "<br>"
                //+ "My Gradual Rollout By User Strategy: "+unleashFeatureFlags.isToggleActiveByUser("MY_GRADUAL_ROLLOUT") + "<br>"
                ;
    }

}
