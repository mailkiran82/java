package org.mailkiran82.springboot.featureflagdemo.unleash;

import no.finn.unleash.DefaultUnleash;
import no.finn.unleash.Unleash;
import no.finn.unleash.UnleashContext;
import no.finn.unleash.util.UnleashConfig;
import org.springframework.stereotype.Service;

@Service
public class UnleashFeatureFlags {
    private Unleash unleash;

    public UnleashFeatureFlags() {
        UnleashConfig config = UnleashConfig.builder()
                .appName("feature-flag-demo")
                .instanceId("12345")
                .fetchTogglesInterval(1)
                .unleashAPI("http://localhost:4242/api/")
                .build();

        unleash = new DefaultUnleash(config);
    }

    public boolean isToggleActive(final String feature) {
        return unleash.isEnabled(feature);
    }

    public boolean isToggleActiveByUser(final String feature, final String id) {
        UnleashContext context = UnleashContext.builder()
                .userId(id).build();

        return unleash.isEnabled(feature, context);

    }


}
