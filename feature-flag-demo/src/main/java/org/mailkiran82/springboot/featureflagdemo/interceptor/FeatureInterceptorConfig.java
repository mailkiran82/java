package org.mailkiran82.springboot.featureflagdemo.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Component
public class FeatureInterceptorConfig extends WebMvcConfigurationSupport {

    @Autowired
    FeatureInterceptor featureInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registery){
        registery.addInterceptor(featureInterceptor);
    }
}
