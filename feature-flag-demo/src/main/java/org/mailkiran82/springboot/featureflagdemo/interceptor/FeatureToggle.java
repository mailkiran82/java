package org.mailkiran82.springboot.featureflagdemo.interceptor;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FeatureToggle {
    String feature();

    boolean expectedToBeOn() default false;
}
