package org.bitbucket.mailkiran82.MarketOpenTrendsWithJoin;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;

import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class MarketOpenTrendsWithJoin {

    public static class Map extends Mapper<LongWritable,Text,Text,Text> {
        public void map(LongWritable key, Text value, Context context ) throws IOException, InterruptedException {

            String line  = value.toString();
            String[] fields = line.split(",");

            String SERIES = fields[1];
            String OPEN = fields[2];
            String PREVCLOSE = fields[7];
            String TIMESTAMP = fields[10];

            if(SERIES.equals("EQ")){
                Double opMinusCp = Double.parseDouble(OPEN) - Double.parseDouble(PREVCLOSE);
                context.write(new Text(TIMESTAMP), new Text("SUM:"+opMinusCp.toString()));
            }
        }
    }

    public static class MapDays extends Mapper<LongWritable,Text,Text,Text> {
        public void map(LongWritable key, Text value, Context context ) throws IOException, InterruptedException {

            String line  = value.toString();
            String[] fields = line.split(",");

            String TIMESTAMP = fields[0];
            String DAY = fields[1];

            context.write(new Text(TIMESTAMP), new Text("DAY:"+DAY));
        }
    }


    public static class Reduce extends Reducer<Text,Text,Text,DoubleWritable>{
        public void reduce(Text key, Iterable<Text > value, Context context) throws IOException, InterruptedException {
            Double sum = 0.0;
            String day = "";
            for(Text x : value){
                String[] line = x.toString().split(":");
                if(line[0].equals("SUM")){
                    sum += Double.parseDouble(line[1]);
                }
                else if (line[0].equals("DAY")){
                    day = line[1];
                }
            }

            key.set(key+"("+day+")");
            context.write(key, new DoubleWritable(sum));
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        System.out.println("Computing...");

        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf, "MarketOpenTrendsWithJoin");

        job.setJarByClass(MarketOpenTrendsWithJoin.class);
//        job.setMapperClass(Map.class);

        MultipleInputs.addInputPath(job, new Path(args[0]),TextInputFormat.class,Map.class);
        MultipleInputs.addInputPath(job, new Path(args[1]),TextInputFormat.class,MapDays.class);

        job.setReducerClass(Reduce.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        //job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class );

        Path outputPath = new Path(args[2]);

        //FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[2]));

        outputPath.getFileSystem(conf).delete(outputPath,true);

        System.exit(job.waitForCompletion(true) ? 0 :1 );

    }
}




