package org.bitbucket.mailkiran82.MovieRecommendation;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.fs.FileSystem;

import java.io.*;
import java.net.URI;
import java.util.HashMap;
import java.util.regex.Pattern;

public class Map3 extends Mapper<Text, Text, Text, Text> {

    private BufferedReader bufferedReader;
    private static HashMap<String,String> MoviesHashMap = new HashMap<String,String>();

    enum MYCOUNTER{
        MY_FILE_EXISTS,MY_HASHMAP_SIZE,MY_HASHMAP_COUNTER,MY_FILE_NOT_FOUND_ERROR,MY_FILE_OTHER_ERROR,MY_DEBUG0,MY_DEBUG00,MY_DEBUG1
    }

    @Override
    public void setup(Context context ) throws IOException, InterruptedException {
        super.setup(context);
        URI[] files = context.getCacheFiles();

        Path filePath = new Path(files[0]);
        if(filePath.getName().trim().equals("u.item") ){
            context.getCounter(MYCOUNTER.MY_FILE_EXISTS).setValue(1);
            loadMovieNameDaysHashMap(filePath, context);
            context.getCounter(MYCOUNTER.MY_HASHMAP_SIZE).setValue(MoviesHashMap.size());
        }
    }

    private void loadMovieNameDaysHashMap(Path filePath, Context context) throws IOException {
        String lineRead ="";
        try {
            FileSystem fs = FileSystem.get(context.getConfiguration());
            bufferedReader = new BufferedReader(new InputStreamReader(fs.open(filePath)));
            while( (lineRead = bufferedReader.readLine()) != null){
                String[] l = lineRead.split(Pattern.quote("|"));
                MoviesHashMap.put(l[0].trim(),l[1].trim());
                //context.getCounter(MYCOUNTER.MY_DEBUG1).setValue(Long.parseLong(l[0].trim()));
                //context.getCounter(MYCOUNTER.MY_HASHMAP_COUNTER).increment(1);
            }
        } catch (FileNotFoundException e) {
            context.getCounter(MYCOUNTER.MY_FILE_NOT_FOUND_ERROR).setValue(1);
            e.printStackTrace();
        } catch (Exception e) {
            context.getCounter(MYCOUNTER.MY_FILE_OTHER_ERROR).setValue(1);
            e.printStackTrace();
        }
        finally {
            if(bufferedReader != null){
                bufferedReader.close();
            }
        }
    }


    public void map(Text key, Text value, Context context ) throws IOException, InterruptedException {
        //get (movie1, movie2) => (score,numPair)
        //return (movie1,score) => (movie2, numPair)
        String[] movies = key.toString().split(":");
        String movie1 = movies[0];
        String movie2 = movies[1];

        String[] values = value.toString().split(":");
        String score = values[0];
        String numPair = values[1];

        //context.getCounter(MYCOUNTER.MY_DEBUG0).setValue(Long.parseLong(movie1.trim()));
        //context.getCounter(MYCOUNTER.MY_DEBUG00).seValue(Long.parseLong(movie2.trim()));
        String movieName1 = MoviesHashMap.get(movie1.trim());
        String movieName2 = MoviesHashMap.get(movie2.trim());

        context.write(new Text(movieName1+"#"+score), new Text(movieName2+"#"+numPair));
    }
}
