package org.bitbucket.mailkiran82.MovieRecommendation;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

import static java.lang.Math.sqrt;

public class Reduce2 extends Reducer<Text, Text, Text, Text>{

    public void reduce(Text key, Iterable<Text> value, Context context) throws IOException, InterruptedException {
        //get every pair of movies watched by all the users, along with its rating.
        //measure the similarity based on the rating given by the all the users who has watched that pair of movie.
        //return movie pair(key as it is)  => (score,co-ratings)
        //compute Cosine similarity metric between two rating vector/lists.
        Integer numPairs = 0;
        Double score = 0.0;
        Double sum_XX = 0.0;
        Double sum_YY = 0.0;
        Double sum_XY = 0.0;

        for( Text r: value){
            String[] ratingPair = r.toString().split(":");
            Integer ratingX = Integer.parseInt(ratingPair[0]);
            Integer ratingY = Integer.parseInt(ratingPair[1]);

            sum_XX += ratingX * ratingX;
            sum_YY += ratingY * ratingY;
            sum_XY += ratingX * ratingY;

            numPairs +=1;

            Double numerator = sum_XY;
            Double denominator = Math.sqrt(sum_XX) * Math.sqrt(sum_YY);

            if(denominator > 0.0){
                score = (numerator/denominator);
            }
        }

        //output only of more than 100 users have watched the movie pair and their rating similarity is high (>95%)
        if( numPairs > 100 && score > 0.95){
            context.write(key, new Text(score.toString()+":"+numPairs.toString()));
        }

    }
}
