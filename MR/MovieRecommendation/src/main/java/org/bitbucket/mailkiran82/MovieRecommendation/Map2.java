package org.bitbucket.mailkiran82.MovieRecommendation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Map2 extends Mapper<Text, Text, Text, Text> {
    public void map(Text key, Text value, Context context ) throws IOException, InterruptedException {
        //get UserID = > List[(MovieID, Rating)]
        //For a given UserID return (MovieID1,MovieID2) => (Rating1, Rating2)

        List<String> items = Arrays.asList(value.toString()
                .replace("[","")
                .replace("]","")
                .split(","));

        //For a UsserID, generate all the all the combination for movies he has watched and rated.
        for( int i =0 ; i < items.size() -1 ; i++) {
            for (int j = i + 1; j < items.size(); j++) {
                String i_movieRating = items.get(i);
                String j_movieRating = items.get(j);

                String[] i_movieAndRating = i_movieRating.split(":");
                String[] j_movieAndRating = j_movieRating.split(":");

                String i_movie = i_movieAndRating[0];
                String i_rating = i_movieAndRating[1];

                String j_movie = j_movieAndRating[0];
                String j_rating = j_movieAndRating[1];

                context.write(new Text(i_movie+":"+j_movie), new Text(i_rating+":"+j_rating));
                context.write(new Text(j_movie+":"+i_movie), new Text(j_rating+":"+i_rating));

            }
        }
    }
}
