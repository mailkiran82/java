package org.bitbucket.mailkiran82.MovieRecommendation;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.*;

public class Map1 extends Mapper<LongWritable, Text, LongWritable, Text> {
    public void map(LongWritable key, Text value, Context context ) throws IOException, InterruptedException {
        //return UserID => (MovieID, Rating)
        String line  = value.toString();
        String[] fields = line.split("\t");

        String userID = fields[0];
        String movieID = fields[1];
        String rating = fields[2];
        String timestamp = fields[3];

        Long uid = Long.parseLong(userID);
            //String day = DaysHashMap.get(TIMESTAMP);
        context.write(new LongWritable(uid), new Text(movieID+":"+rating));
    }
}
