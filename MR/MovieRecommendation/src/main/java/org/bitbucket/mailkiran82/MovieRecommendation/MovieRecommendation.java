package org.bitbucket.mailkiran82.MovieRecommendation;

import java.io.IOException;
import java.net.URI;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;

import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class MovieRecommendation extends Configured implements Tool {

    public static void main(String[] args) throws Exception {

        int exitStatus = ToolRunner.run(new Configuration()
                ,new MovieRecommendation(), args);
    }

    public int run(String[] args) throws Exception {
        System.out.println("Running...");

        Configuration conf = new Configuration();

        Path outputPath = new Path(args[2]);
        outputPath.getFileSystem(conf).delete(outputPath,true);

        Job job = Job.getInstance(conf, "MovieRecommendation");
        job.setJarByClass(MovieRecommendation.class);
        job.setMapperClass(Map1.class);
        job.setReducerClass(Reduce1.class);
        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(Text.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class );
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[2]+"/tempfile1"));
        job.waitForCompletion(true);


        Configuration conf1 = new Configuration();
        Job job1 = Job.getInstance(conf1, "MovieRecommendation_process");
        job1.setJarByClass(MovieRecommendation.class);
        job1.setMapperClass(Map2.class);
        job1.setReducerClass(Reduce2.class);
        job1.setMapOutputKeyClass(Text.class);
        job1.setMapOutputValueClass(Text.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(Text.class);
        job1.setInputFormatClass(KeyValueTextInputFormat.class);
        job1.setOutputFormatClass(TextOutputFormat.class );
        FileInputFormat.addInputPath(job1, new Path(args[2]+"/tempfile1"));
        FileOutputFormat.setOutputPath(job1, new Path(args[2]+"/tempfile2"));
        job1.waitForCompletion(true);

        Configuration conf2 = new Configuration();
        Job job2 = Job.getInstance(conf1, "MovieRecommendation_process");

        Path daysPath = new Path(args[1]);
        job2.addCacheFile(daysPath.toUri());

        job2.setJarByClass(MovieRecommendation.class);
        job2.setMapperClass(Map3.class);
        job2.setReducerClass(Reduce3.class);
        job2.setMapOutputKeyClass(Text.class);
        job2.setMapOutputValueClass(Text.class);
        job2.setOutputKeyClass(Text.class);
        job2.setOutputValueClass(Text.class);
        job2.setInputFormatClass(KeyValueTextInputFormat.class);
        job2.setOutputFormatClass(TextOutputFormat.class );
        FileInputFormat.addInputPath(job2, new Path(args[2]+"/tempfile2"));
        FileOutputFormat.setOutputPath(job2, new Path(args[2]+"/final"));

        boolean returnStatus = job2.waitForCompletion(true);
        return returnStatus ? 0 :1;
    }
}




