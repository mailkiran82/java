package org.bitbucket.mailkiran82.MovieRecommendation;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Reduce3 extends Reducer<Text, Text, Text, Text >{

    public void reduce(Text key, Iterable<Text> value, Context context) throws IOException, InterruptedException {
        //get (movie1,score) => (movie2,numPair)
        //write (movie) -> (movie2,score,numPair)
        String[] keyFields = key.toString().split("#");
        String movie1 = keyFields[0];
        String score = keyFields[1];

        for(Text t : value) {
            String[] valuesFields = t.toString().split("#");
            String movie2 = valuesFields[0];
            String numPair = valuesFields[1];
            context.write(new Text(movie1+"=>"), new Text(movie2+":"+score+":"+numPair));
        }
    }
}
