package org.bitbucket.mailkiran82.MovieRecommendation;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Reduce1 extends Reducer<LongWritable, Text, LongWritable, Iterable<Text> >{

    public void reduce(LongWritable key, Iterable<Text> value, Context context) throws IOException, InterruptedException {
        //get UserID => (MovieID, Rating)
        //return UserID = > List[(MovieID, Rating)]

        List<Text> list = new ArrayList<Text>();

        for(Text x : value){
            list.add(new Text(x));
        }

        context.write(key, list);
    }
}
