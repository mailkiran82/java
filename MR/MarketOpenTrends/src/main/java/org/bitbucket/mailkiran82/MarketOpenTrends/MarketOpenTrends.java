package org.bitbucket.mailkiran82.MarketOpenTrends;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;

import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class MarketOpenTrends {

    public static class Map extends Mapper<LongWritable,Text,Text,DoubleWritable> {
        public void map(LongWritable key, Text value, Context context ) throws IOException, InterruptedException {

            final Log log = LogFactory.getLog(Map.class);

            String line  = value.toString();
            String[] fields = line.split(",");

            String SERIES = fields[1];
            String OPEN = fields[2];
            String PREVCLOSE = fields[7];
            String TIMESTAMP = fields[10];

            if(SERIES.equals("EQ")){
                Double opMinusCp = Double.parseDouble(OPEN) - Double.parseDouble(PREVCLOSE);
                context.write(new Text(TIMESTAMP), new DoubleWritable(opMinusCp));
            }
        }
    }

    public static class Reduce extends Reducer<Text,DoubleWritable,Text,DoubleWritable>{
        public void reduce(Text key, Iterable<DoubleWritable> value, Context context) throws IOException, InterruptedException {
            Double sum = 0.0;
            for(DoubleWritable x : value){
                sum += x.get();
            }

            Text trend = new Text("Bearish : ");
            if(sum > 0.0)
                trend = new Text("Bullish : ");

            key.set(key + " : " + trend);
            context.write(key, new DoubleWritable(sum));
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        System.out.println("Computing...");

        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf, "MRMarketOpenTrend");

        job.setJarByClass(MarketOpenTrends.class);
        job.setMapperClass(Map.class);
        job.setReducerClass(Reduce.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(DoubleWritable.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class );

        Path outputPath = new Path(args[1]);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        outputPath.getFileSystem(conf).delete(outputPath,true);

        System.exit(job.waitForCompletion(true) ? 0 :1 );

    }
}




