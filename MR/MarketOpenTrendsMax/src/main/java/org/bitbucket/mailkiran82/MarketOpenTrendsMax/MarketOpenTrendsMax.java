package org.bitbucket.mailkiran82.MarketOpenTrendsMax;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;

import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class MarketOpenTrendsMax {

    public static class Map extends Mapper<LongWritable,Text,Text,DoubleWritable> {
        public void map(LongWritable key, Text value, Context context ) throws IOException, InterruptedException {

            final Log log = LogFactory.getLog(Map.class);

            String line  = value.toString();
            String[] fields = line.split(",");

            String SERIES = fields[1];
            String OPEN = fields[2];
            String PREVCLOSE = fields[7];
            String TIMESTAMP = fields[10];

            if(SERIES.equals("EQ")){
                Double opMinusCp = Double.parseDouble(OPEN) - Double.parseDouble(PREVCLOSE);
                context.write(new Text(TIMESTAMP), new DoubleWritable(opMinusCp));
            }
        }
    }

    public static class Reduce extends Reducer<Text,DoubleWritable,Text,DoubleWritable>{
        public void reduce(Text key, Iterable<DoubleWritable> value, Context context) throws IOException, InterruptedException {
            Double sum = 0.0;
            for(DoubleWritable x : value){
                sum += x.get();
            }

            Text trend = new Text("Bearish )");
            if(sum > 0.0)
                trend = new Text("Bullish )");

            key.set(key + " ( " + trend);
            context.write(key, new DoubleWritable(sum));
        }
    }

    public static class Map1 extends Mapper<Text, Text, Text, Text>{
        public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
            context.write(new Text("ALL"),new Text(key + ":" + value));
        }
    }

    public static class Reduce1 extends Reducer<Text, Text, Text, DoubleWritable>{
        public void reduce(Text key, Iterable<Text> value, Context context) throws IOException, InterruptedException {
            String maxDate = "";
            Double maxPrice = Double.MIN_VALUE;
            for( Text x : value){
                String[] tokens = x.toString().split(":");
                String date = tokens[0];
                Double price = Double.parseDouble(tokens[1]);

                if(price > maxPrice){
                    maxPrice = price;
                    maxDate = date;
                }
            }

            context.write(new Text(maxDate), new DoubleWritable(maxPrice));
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        System.out.println("Computing...");

        Configuration conf = new Configuration();

        Path outputPath = new Path(args[1]);
        outputPath.getFileSystem(conf).delete(outputPath,true);

        Job job = Job.getInstance(conf, "MarketOpenTrends");
        job.setJarByClass(MarketOpenTrendsMax.class);
        job.setMapperClass(Map.class);
        //job.setCombinerClass(Reduce.class);
        job.setReducerClass(Reduce.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(DoubleWritable.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class );
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]+"/tempfile"));
        job.waitForCompletion(true);

        Configuration conf1 = new Configuration();
        Job job1 = Job.getInstance(conf1, "MarketOpenTrendsMax");
        job1.setJarByClass(MarketOpenTrendsMax.class);
        job1.setMapperClass(Map1.class);
        //job1.setCombinerClass(Reduce1.class);
        job1.setReducerClass(Reduce1.class);
        job1.setMapOutputKeyClass(Text.class);
        job1.setMapOutputValueClass(Text.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(DoubleWritable.class);
        job1.setInputFormatClass(KeyValueTextInputFormat.class);
        job1.setOutputFormatClass(TextOutputFormat.class );
        FileInputFormat.addInputPath(job1, new Path(args[1]+"/tempfile"));
        FileOutputFormat.setOutputPath(job1, new Path(args[1]+"/final"));

        System.exit(job1.waitForCompletion(true) ? 0 :1 );
    }
}




