package org.bitbucket.mailkiran82.MarketOpenTrendsWithChain;

import java.io.IOException;
import java.nio.channels.FileChannel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;

import org.apache.hadoop.mapreduce.lib.chain.ChainMapper;
import org.apache.hadoop.mapreduce.lib.chain.ChainReducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class MarketOpenTrendsWithChain {

    public static class MapFilter extends Mapper<LongWritable,Text,LongWritable,Text> {
        public void map(LongWritable key, Text value, Context context ) throws IOException, InterruptedException {
            String line  = value.toString();
            String[] fields = line.split(",");
            String SERIES = fields[1];
            if(SERIES.equals("EQ")){
                context.write(key,new Text(line));
            }
        }
    }
    public static class MapCompute extends Mapper<LongWritable,Text,Text,DoubleWritable> {
        public void map(LongWritable key, Text value, Context context ) throws IOException, InterruptedException {
            String line  = value.toString();
            String[] fields = line.split(",");

            String OPEN = fields[2];
            String PREVCLOSE = fields[7];
            String TIMESTAMP = fields[10];

            Double opMinusCp = Double.parseDouble(OPEN) - Double.parseDouble(PREVCLOSE);
            context.write(new Text(TIMESTAMP), new DoubleWritable(opMinusCp));
        }
    }

    public static class MapMax extends Mapper<Text,Text,Text,DoubleWritable> {
        public void map(Text key, Iterable<Text> value, Context context ) throws IOException, InterruptedException {

            Double maxValue = 0.0;
            String maxDate = "";
            for(Text v : value) {

                String line  = v.toString();
                String[] fields = line.split(":");

                String date = fields[0];
                Double price = Double.parseDouble(fields[1]);

                if(price > maxValue){
                    maxValue = price;
                    maxDate = date;
                }

                //context.write(new Text(maxDate), new DoubleWritable(maxValue));
            }
            context.write(new Text(maxDate), new DoubleWritable(maxValue));
        }
    }


    public static class Reduce extends Reducer<Text,DoubleWritable,Text,DoubleWritable>{
        public void reduce(Text key, Iterable<DoubleWritable> value, Context context) throws IOException, InterruptedException {
            Double sum = 0.0;
            for(DoubleWritable x : value){
                sum += x.get();
            }

            Text trend = new Text("Bearish)");
            if(sum > 0.0)
                trend = new Text("Bullish)");

            key.set(key + " (" + trend);
            context.write(key, new DoubleWritable(sum));
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        System.out.println("Computing...");

        Configuration conf = new Configuration();

        Path outputPath = new Path(args[1]);
        outputPath.getFileSystem(conf).delete(outputPath,true);

        Job job = Job.getInstance(conf, "MarketOpenTrends");

        Configuration mapFilterConf = new Configuration(false);
        ChainMapper.addMapper(job,MapFilter.class,LongWritable.class,Text.class,LongWritable.class,Text.class,mapFilterConf);
        Configuration mapComputeConf = new Configuration(false);
        ChainMapper.addMapper(job,MapCompute.class,LongWritable.class,Text.class,Text.class,DoubleWritable.class,mapComputeConf);

        Configuration reduceConf = new Configuration(false);
        ChainReducer.setReducer(job, Reduce.class, Text.class ,DoubleWritable.class ,Text.class,DoubleWritable.class,reduceConf);


        job.setJarByClass(MarketOpenTrendsWithChain.class);
        //job.setMapperClass(Map.class);
        //job.setCombinerClass(Reduce.class);
        //job.setReducerClass(Reduce.class);
        //job.setOutputKeyClass(Text.class);
        //job.setOutputValueClass(DoubleWritable.class);
        //job.setInputFormatClass(TextInputFormat.class);
        //job.setOutputFormatClass(TextOutputFormat.class );
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));


        System.exit(job.waitForCompletion(true) ? 0 :1 );
    }
}




