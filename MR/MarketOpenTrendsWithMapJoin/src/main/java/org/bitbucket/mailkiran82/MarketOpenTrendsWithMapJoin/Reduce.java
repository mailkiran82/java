package org.bitbucket.mailkiran82.MarketOpenTrendsWithMapJoin;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class Reduce extends Reducer<Text, DoubleWritable,Text,DoubleWritable> {
        public void reduce(Text key, Iterable<DoubleWritable> value, Context context) throws IOException, InterruptedException {
            Double sum = 0.0;
            for(DoubleWritable x : value){
                sum += x.get();
            }

            Text trend = new Text("Bearish )");
            if(sum > 0.0)
                trend = new Text("Bullish )");

            key.set(key + " ( " + trend);
            context.write(key, new DoubleWritable(sum));
        }
    }
