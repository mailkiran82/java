package org.bitbucket.mailkiran82.MarketOpenTrendsWithMapJoin;

import java.io.IOException;
import java.net.URI;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;

import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class MarketOpenTrendsWithMapJoin extends Configured implements Tool {

    public static void main(String[] args) throws Exception {

        int exitStatus = ToolRunner.run(new Configuration()
        ,new MarketOpenTrendsWithMapJoin(), args);
    }

    public int run(String[] args) throws Exception {
        System.out.println("Running...");

        Configuration conf = new Configuration();

        Path outputPath = new Path(args[2]);
        outputPath.getFileSystem(conf).delete(outputPath,true);

        Job job = Job.getInstance(conf, "MarketOpenTrends");

        Path daysPath = new Path(args[1]);
        job.addCacheFile(daysPath.toUri());

        job.setJarByClass(MarketOpenTrendsWithMapJoin.class);
        job.setMapperClass(Map.class);
        job.setReducerClass(Reduce.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(DoubleWritable.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class );
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        job.waitForCompletion(true);

        boolean returnStatus = job.waitForCompletion(true);
        return returnStatus ? 0 :1;
    }
}




