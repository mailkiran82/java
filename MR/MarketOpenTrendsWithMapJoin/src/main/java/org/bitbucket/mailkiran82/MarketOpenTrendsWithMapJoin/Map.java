package org.bitbucket.mailkiran82.MarketOpenTrendsWithMapJoin;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.*;
import java.net.URI;
import java.util.HashMap;

public class Map extends Mapper<LongWritable, Text,Text, DoubleWritable> {

    private BufferedReader bufferedReader;
    private static HashMap<String,String> DaysHashMap = new HashMap<String,String>();

    enum MYCOUNTER{
        MY_FILE_EXISTS,MY_DAYS_HASHMAP_SIZE,MY_FILE_NOT_FOUND_ERROR,MY_FILE_OTHER_ERROR
    }

    @Override
    public void setup(Context context ) throws IOException, InterruptedException {
        super.setup(context);
        URI[] files = context.getCacheFiles();

        Path filePath = new Path(files[0]);
        if(filePath.getName().trim().equals("Days.csv") ){
            context.getCounter(MYCOUNTER.MY_FILE_EXISTS).setValue(1);
            loadDaysHashMap(filePath, context);
            context.getCounter(MYCOUNTER.MY_DAYS_HASHMAP_SIZE).setValue(DaysHashMap.size());
        }
    }

    private void loadDaysHashMap(Path filePath, Context context) throws IOException {
        String lineRead ="";
        try {
            FileSystem fs = FileSystem.get(context.getConfiguration());
            bufferedReader = new BufferedReader(new InputStreamReader(fs.open(filePath)));
            while( (lineRead = bufferedReader.readLine()) != null){
                String[] l = lineRead.split(",");
                DaysHashMap.put(l[0].trim(),l[1].trim());
            }
        } catch (FileNotFoundException e) {
            context.getCounter(MYCOUNTER.MY_FILE_NOT_FOUND_ERROR).setValue(1);
            e.printStackTrace();
        } catch (Exception e) {
            context.getCounter(MYCOUNTER.MY_FILE_OTHER_ERROR).setValue(1);
            e.printStackTrace();
        }
        finally {
            if(bufferedReader != null){
                bufferedReader.close();
            }
        }
    }

    public void map(LongWritable key, Text value, Context context ) throws IOException, InterruptedException {

        String line  = value.toString();
        String[] fields = line.split(",");

        String SERIES = fields[1];
        String OPEN = fields[2];
        String PREVCLOSE = fields[7];
        String TIMESTAMP = fields[10];

        if(SERIES.equals("EQ")){
            Double opMinusCp = Double.parseDouble(OPEN) - Double.parseDouble(PREVCLOSE);
            String day = DaysHashMap.get(TIMESTAMP);
            context.write(new Text(TIMESTAMP + " (" + day + ") "), new DoubleWritable(opMinusCp));
        }
    }
}
