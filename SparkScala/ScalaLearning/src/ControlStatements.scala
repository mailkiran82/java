
object ControlStatements {
  
  
  def main(args:Array[String]){
    //if else
    if ( 1 > 2 ){
      println("False")
    }
    else{
      println("True")
    }

  
    //match
    //does not require a break after each case
    val number:Int = 3
    val m: Int =3
    number match{
      case 1 => println("One")
      case 2 => println("Two")
      case m => println("Three") // unlike java/c++ m doesnt have to be a compile time constant
      case _ => println("default")
    }
  
    //for loop using range operator (<-)
    for(x <- 1 to 4){
      println(s"${x*x}")
    }

    //while
    var x=5
    while( x>=0) {
      println(x)
      x -= 1
    }
  
    //do while loop
    var y=0
    do{
      println(y)
      y += 1
    } while (y < 5)

    //expression
      println(s"${val x =10 ; x+20}")
      println({val x =10 ; x+20})
      
      
      
      
      
      
  }
    
}