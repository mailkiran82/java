

object DataTypes {
  
  def main(args:Array[String]){
    
    // val => immutable
    val name:String = "scala"
    println(name)
    
    //var = > mutable
    var fullname:String = ""
    fullname = name+"scala"
    println(fullname)
    
    //use val as much as possible in scala
    // since it a functional language, to avoid the function changing the value use immutable
    //the above var can be converted to val as below
    val fullname1:String = name+"scala"
    println(fullname1)
    
    //other data types
    val number:Int =1
    println(number)
    val bigNumber:Long = 123456789
    println(bigNumber)
    val smallNumber:Byte = 123
    println(smallNumber)
    val double:Double = 1.123456789
    println(double)
    val float:Float = 1.123456789f
    println(float)
    val char:Char ='a'
    println(char)
    val bool:Boolean = true
    println(bool)
    
    //concat
    println("concat string"+number+" "+double+" "+float+" "+char+" "+bool)
    
    //substitute a variable
    println(s"here is a number and boolean: $number and $bool")
    
    //substitute a expression 
    println(s"here is a number with expression : ${number+number}")

    //printf style
    println(f"double precision : $double%.3f")
    println(f"padding number : $smallNumber%05d")
    
    //regular expression
    val myString:String = "my ph # is 123, and pin code is 456."
    val pattern = """.* ([\d]+).*""".r
    val pattern(answerString) = myString
    val answer = answerString.toInt
    println(answer)
    
    val isGreater = 1>2
    println(isGreater)
    val isLesser = 1<2
    println(isLesser)
    val impossible = isGreater & isLesser
    println(impossible)
    val anotherWay = isGreater && isLesser
    println(anotherWay)
    
    //string comparision
    val str1:String = "scala"
    val str2:String = "scala"
    val bool1:Boolean = (str1 == str2)
    println(bool1)
  }
  
}