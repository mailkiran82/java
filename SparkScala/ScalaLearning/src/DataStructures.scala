

object DataStructures {
    
  def main(args:Array[String]){
    
    //tuples , immutable list 
    
    val tuple = ("abc","def","ghi")
    println(tuple)
    println(tuple._1) //starts from index _1
    println(tuple._3)
    
    //key value pair
    val keyValue = "key" -> "value" //keyValue:(String,String) = ("key","value")
    println(keyValue._2)
    
    val mixedBag = ("abc",1,10.00,true) //mixedBag:(String,Int,Double,Boolean)
    println(mixedBag._1)
    println(mixedBag._2)
    println(mixedBag._3)
    println(mixedBag._4)
    
    //List
    //creates a singly linked list
    val myList = List("abc","def","ghi")
    println(myList(0))
    println(myList.head)
    println(myList.tail)
    
    //iterate through the list
    for(x <- myList) { println(x) }
    
    //print the items in reverse
    //map function transforms a list into a new list
    val reverseMyList = myList.map( (x:String) => (x.reverse))
    for(x <- reverseMyList) { println(x)}
    
    // reduce is used to combine all the items in a list
    val intList = List(1,2,3,4,5)
    val sum = intList.reduce( (x:Int, y:Int) => x+y)
    println(sum)
    
    val dropFour = intList.filter( (x:Int) => x!= 4)
    println(dropFour)

    val dropTwo = intList.filter( _ != 2)
    println(dropTwo)    
    
    //concat list
    val intList1 = List(6,7,8,9,10)
    val intList2 = intList ++ intList1
    println(intList2)
    
    //reverse
    val reverseList = intList2.reverse
    println(reverseList)
    
    val sortedList = intList2.sorted
    println(sortedList)
    
    val listWithDuplicate = intList ++ List(1,2,3)
    val listUnique = listWithDuplicate.distinct
    println(listUnique)
    
    println(intList.max)
    println(intList.min)
    println(intList.sum)
    println(intList.contains(5))
    
    //maps/dictionaries
    val numMap = Map(1 -> "One", 2 -> "Two", 3 -> "Three", 4 -> "Four", 5 -> "Five")
    println(numMap)
    println(numMap(1))
    println(numMap.contains(6))
    
    val four = util.Try(numMap(6)) getOrElse "NA"
    println(four)
    
    
    
    
    
    
  }
}