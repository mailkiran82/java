object Functions {
  
  def main(args:Array[String]){

    //it is like an expression., hence it does not require an return statement
    //the last expression that is evaluated is returned.
    // = {} represents as an object is assigned 
    def squareIt(x:Int):Int = {
      x*x
    }
    
    println(squareIt(2))
    
    //pass function a parameter to another function
    // the second parameter is a function which takes one Int value and return an Int as outout
    def transformInt(x:Int, f:Int => Int ): Int = {
      f(x)
    }
    
    val result = transformInt(2,squareIt)
    println(result)
    
    // anonymous function or lamba function
    println(transformInt(2, x => x*x))
    
    println(transformInt(2, x=> { val y = x*x ; y*x }))
  
  
  }
 }