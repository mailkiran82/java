package org.bitbucket.mailkiran82.spark

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object MovieRatingCounter {
  
  def main(args: Array[String]){
    
    // set the log level to print on only errors
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    //create a spark context using every core on the local machine, named MovieRatingCounter
    val sc = new SparkContext("local[*]","MovieRatingCounter")
    
    //load up each line of the ratings data into an RDD
    val lines = sc.textFile("../ml-100k/u.data")
    
    //convert each line to a string, split it out by tabs, and extract the third field
    // (the file format is userID, movieid, rating, timestamp)
    val ratings = lines.map(x=> x.toString().split("\t")(2))
    
    // count up how any timing each value (rating) occurs
    val results = ratings.countByValue()
    
    //sort the resulting map of rating, count tuples
    val sortedResults = results.toSeq.sortBy( x => (x._1,x._2))
    
    //print each result on its own line.
    sortedResults.foreach(println)
  }
  
}