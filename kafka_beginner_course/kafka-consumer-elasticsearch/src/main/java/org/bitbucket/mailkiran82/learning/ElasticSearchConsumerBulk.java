package org.bitbucket.mailkiran82.learning;

import com.google.gson.JsonParser;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class ElasticSearchConsumerBulk {

    public static RestHighLevelClient createClient(){

        String hostName = "";
        String userName = "";
        String pwd = "";

        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(userName,pwd));

        RestClientBuilder builder = RestClient.builder(new HttpHost(hostName, 9200, "http"));

        builder.setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
            @Override
            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
            }
        });

        RestHighLevelClient client = new RestHighLevelClient((builder));
        return client;
    }

    public static KafkaConsumer<String, String> createKafkaConsumer(String topic){
        // create Consumer Properties
        String bootstrapServerConfig = "127.0.0.1:9092";
        String groupID = "kafka-demo-elasticsearch";

        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServerConfig);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG,groupID);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");
        properties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,"false");  //disable auto commit of offset
        properties.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG,"10"); //get 10 records at a time

        //create Consumer
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);
        consumer.subscribe(Arrays.asList(topic));

        return consumer;
    }


    private static JsonParser jsonParser = new JsonParser();

    private static String extractIDFromJson(String tweet){
        //using gson
        return jsonParser.parse(tweet).getAsJsonObject().get("id_str").getAsString();
    }

    public static void main(String[] args) throws IOException {
        Logger logger = LoggerFactory.getLogger(ElasticSearchConsumerBulk.class);
        RestHighLevelClient client = createClient();


        String topic = "twitter_tweet";
        KafkaConsumer<String, String> consumer = createKafkaConsumer(topic);

        //poll
        while(true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));

            Integer recordCount = records.count();
            System.out.println("Received " + recordCount + "records");

            BulkRequest bulkRequest = new BulkRequest();

            for( ConsumerRecord<String, String> record : records  ) {
                //insert into elastic search
                String jsonString = record.value();
                //2 strategies
                //1 ) kafka generic id
                //String id = record.topic() + "_" + record.partition() + "_" + record.offset();

                //2 ) twitter specific id from the twitter message
                try {
                    String id = extractIDFromJson(record.value());

                    IndexRequest indexRequest = new IndexRequest("twitter", "tweet", id) // id is used to make the consumer idempotent
                            .source(jsonString, XContentType.JSON);

                    bulkRequest.add(indexRequest);
                }
                catch(NullPointerException e){
                    System.out.println(" Bad data, skipping tweet : " + record.value());
                }
            }

            if (recordCount > 0) {
                BulkResponse bulkItemResponses = client.bulk(bulkRequest, RequestOptions.DEFAULT);

                System.out.println("Committing offsets ...");
                consumer.commitSync();
                System.out.println("Committed offsets.");

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        //client.close();

    }
}
