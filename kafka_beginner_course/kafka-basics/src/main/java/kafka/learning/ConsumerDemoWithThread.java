package kafka.learning;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class ConsumerDemoWithThread {
    public static void main(String[] args) {
        new ConsumerDemoWithThread().run();
    }

    private ConsumerDemoWithThread() {
    }

    private void run() {
        Logger logger = LoggerFactory.getLogger(ConsumerDemoWithThread.class);

        // create Consumer Properties
        String bootstrapServerConfig = "127.0.0.1:9092";
        String groupID = "my_second_application";
        String topic = "first_topic";
        CountDownLatch latch = new CountDownLatch(1);

        Runnable myConsumerRunnable = new ConsumerRunnable(
                bootstrapServerConfig,
                groupID,
                topic,
                latch);

        //start he thread
        Thread myThread = new Thread(myConsumerRunnable);
        myThread.start();


        //shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            logger.info("Caught shutdown hook");
            System.out.println("Caught shutdown hook");
            ((ConsumerRunnable) myConsumerRunnable).shutdown();
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info("Application has exited");
            System.out.println("Application has exited");
        }
        ));

        try {
            latch.await();
        } catch (InterruptedException e) {
            logger.error("Application got interrupted", e);
            System.out.println("Application got interrupted");
        } finally {
            logger.info("Application is closing");
            System.out.println("Application is closing");
        }
    }


    public class ConsumerRunnable implements Runnable{
        Logger logger = LoggerFactory.getLogger(ConsumerRunnable.class);
        private KafkaConsumer<String, String> consumer;
        CountDownLatch latch;
        ConsumerRunnable(String bootstrapServerConfig,
                       String groupID,
                       String topic,
                       CountDownLatch latch){
            Properties properties = new Properties();
            properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServerConfig);
            properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG,groupID);
            properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");

            //create Consumer
            consumer = new KafkaConsumer<String, String>(properties);
            consumer.subscribe(Arrays.asList(topic));

             this.latch = latch;

        }
        @Override
        public void run() {
            //poll
            try {
                while (true) {
                    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord record : records) {
                        System.out.println("Key = " + record.key() + " Value = " + record.value());
                        System.out.println("Partition = " + record.partition() + " Offset = " + record.offset());
                    }
                }
            }
            catch ( WakeupException e ){
                logger.info("Received Shutdown signal");
                System.out.println("Received Shutdown signal");
            }
            finally{
                consumer.close();
                //tell main code we are done with consumer
                latch.countDown();
            }
        }

        public void shutdown(){
            consumer.wakeup(); // throws a WakeUp exception, used t interrupt the poll mentod
        }
    }
}


