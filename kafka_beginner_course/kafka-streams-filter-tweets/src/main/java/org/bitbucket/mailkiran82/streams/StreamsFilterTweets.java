package org.bitbucket.mailkiran82.streams;

import com.google.gson.JsonParser;
import com.sun.org.apache.xerces.internal.impl.xpath.XPath;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;

import java.util.Properties;

public class StreamsFilterTweets {
    private static JsonParser jsonParser = new JsonParser();

    private static Integer extractUserFollowersInTweet(String tweet){
        //using gson
        try {
            return jsonParser.parse(tweet).getAsJsonObject().get("user")
                    .getAsJsonObject().get("followers_count").getAsInt();
        }
        catch(NullPointerException e ){
            return 0;
        }
    }

    public static void main(String[] args) {
        //create properties
        Properties properties = new Properties();
        properties.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG,"demo-kafka-streams");
        properties.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());
        properties.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());

        //create a topology
        StreamsBuilder streamsBuilder = new StreamsBuilder();

        //input topic
        KStream<String, String> inputTopic = streamsBuilder.stream("twitter_tweet");
        KStream<String, String> filteredStream = inputTopic.filter(
                (k, jsonString) -> extractUserFollowersInTweet(jsonString) > 10000
        );
        filteredStream.to("important_tweet");

        //build a topology
        KafkaStreams kafkaStreams = new KafkaStreams(
          streamsBuilder.build(),
                properties
        );

        //start stream application
        kafkaStreams.start();
    }
}
