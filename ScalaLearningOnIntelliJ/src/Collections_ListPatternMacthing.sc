def matchList[T](list:List[T]) = list match {
  case List() => println("empty list")
  case List(_) => println("one element")
  case List(_,_) => println("two element")
}

val l = List(1)
matchList(l)

//iterate a list using match
def printList[T](l:List[T]): Unit = l match{
  case List() => println("done")
  case m =>
    println(m.head)
    printList(m.tail)
}

val l1 = List(1,2,3,4,5)
printList(l1)

//iterate a list using match
def printList1[T](l:List[T]): Unit = l match{
  case Nil => println("done")
  case h :: t =>
    println(h)
    printList1(t)
}

printList1(l1)