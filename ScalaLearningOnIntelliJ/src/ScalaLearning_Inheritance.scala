import java.time.LocalDateTime

import org.jcp.xml.dsig.internal.dom.DOMDigestMethod

class Pet {
  def feed() ={
    "feeting at :" + LocalDateTime.now
  }
  def feed1() ={
    "feeting1 at :" + LocalDateTime.now
  }
}

class Cat extends Pet{
  def hunt() = {
    println("Cat hunts")
  }
}


class Dog extends Pet{
  override def feed(): String = {
    "Dog eating"
  }
  override val feed1 = { //to avoid override in the in child class
    "Dog eating1"
  }

}

object ScalaLearning_Inheritance {
  def main(args: Array[String]) = {
    val c = new Cat
    println(c.feed())

    val p: Pet = new Cat //static type is Pet, and runtime type is Cat
    println(c.feed())

    val d :Pet = new Dog
    println(d.feed())
    println(d.feed1)


  }
}
