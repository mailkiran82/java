//prior ti java 8 only the Future was good for defining an interface for the thread client(main thread)
//but was not very good for the producer of the thread can do.
// with  promise it brings a notion of where the result can be stored.
//promise introduces and additional level of abstraction between producer(thread) and consumer(main thread)

import scala.concurrent.{ExecutionContext, Future}
object ScalaLearning_Concurrency {
def main(args:Array[String]): Unit ={

  println("main")
  implicit val ex = ExecutionContext.global
  // option/try/furure are of same family, they may or may not contain a value
  //option/try  are tiny container
  //future is a tiny collection, hence it has foreach, but it always contain only one result
  //f1.foreach( x => println(x))
  // future has three states success/failed/not finished. in scala you have to de-strucure them in a functional way

  val f1 = Future { Thread.sleep(1000);   50}
  Thread.sleep(2000)
  f1.foreach( x => println(x))
  println("Value:"+f1.value)
  println("--")



  val f2 = Future { Thread.sleep(1000)
    3/0}
  Thread.sleep(2000)
  //does not print anything if it fails
  // the above future creates a Future[Int], hence the below does not show/return anything
  f2.foreach(x => println(x))
  println("Value:"+f2.value)

  //this create Future[Throwable], hence it shows the exception
  // this also acts as callback, it is called ofter the above future finishes
  val ff = f2.failed
  Thread.sleep(2000)
  ff.foreach( c =>println(c))
  println("--")

  val f3 = Future { Thread.sleep(1000)
  50}
  //Thread.sleep(2000)
  //this return another future
  //chaining of future
  val f4  = f3.map( n => n*2)
  Thread.sleep(2000)
  println("Value:"+f4.value)





}

}
