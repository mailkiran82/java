val list = List(
  List(1,2,3),
  List(4,5),
  List(6)
)

list.flatten

list.flatten.filter( c => c == 2)

list.flatten.map( c => c+1)
list.flatten.map( _+1)


val nameList = List(
  List("abc","def","ghi"),
  List("jkl","mno"),
  List("pqr")
)

nameList.flatten.map( c => c.capitalize)
nameList.flatMap( c => c.map( c => c.capitalize))
nameList.flatMap( _.map( _.capitalize))

val l = List("abc","def",4,"ghi")
l.map(
  {
    case s:String => s.capitalize
    case m:Any => ""
  }
)

//collect using partial function
l.collect(
  {
    case s:String => s.capitalize
  }
)

//fold

