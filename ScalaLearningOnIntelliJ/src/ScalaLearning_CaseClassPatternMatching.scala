import java.time.LocalDateTime

object ScalaLearning_CaseClassPatternMatching {

  //case class does not require new, it provides a factory method
  //
  case class CashFlow(amt:Double,
                    cur:String,
                    due:LocalDateTime)


  case class Address(city:String,country:String)
  case class Person(name:String,age:Integer,address:Address)

  def main(args:Array[String]): Unit = {

    // new is not required as close to a value type (case class)
    val c1 = CashFlow(300.0, "USD", LocalDateTime.now)

    c1 match {
      case CashFlow(v, "USD", _) => println("US CashFlow")
      case CashFlow(v, "GBP", _) => println("UK Cashflow")
    }

    val a1 = Address("BLR","India")
    val p1 = Person("ABC",25,a1)
    println(p1)

    p1 match {
      case Person(n,_,Address("BLR",_)) => println(s"$n lives in BLR")
    }

    p1 match {
      case Person(n,a,_) if a > 20 => println(s"$n is greater than 20") // the if condition here is called guards
    }

  }

}
