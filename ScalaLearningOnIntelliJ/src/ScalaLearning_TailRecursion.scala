import scala.annotation.tailrec

object ScalaLearning_TailRecursion {

  def fact(n:Int): Long ={
    n match{
      case 0 => 1
      case m => m * fact(m-1)
    }
  }

  def factorial(n:Int): Long ={
    @tailrec def fact_inner(n:Int, sum:Long): Long ={
      n match{
        case 0 => sum
        case m => fact_inner(m-1, m*sum)
      }
    }

    fact_inner(n,1)
  }

  def main(args:Array[String]): Unit ={

    println(fact(10))
    //println(fact(100000))

    println(factorial(10))
    //println(factorial(100000))

  }

}
