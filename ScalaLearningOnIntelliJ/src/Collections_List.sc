// by default List is immutable
// the type List points to the immutable List is scala
// the list here is a case class. Hence new is not used
//it calls the apply method on the companion class

val list = List(1,2,3)
println(list)

//it right associative
//it starts building from right (Nil)
val list0 = 1 :: 2 :: 3 :: Nil

//add to front
//this gets translated into list.::(0)
//here th e:: method is applied on the rhs
val list1 = 0 :: list

val list2 = list :+ 4

//Nothing means no instances.
val list3 = List()

val list4 = list ::: list0

val list5 = Nil

val lst0 = List ('a','b','c')

val list6 = list ::: lst0

list.head
list.tail
list.init
list.last
