object ScalaLearning_PatternMatching {

  def doIt(x:Any) = x match {
    case _ : Int =>  println("Its a Int")
    case _ : String => println("Its a String")
    case _ => println("Its something else")
  }

  def doIt1(x:Any) = x match {
    case n : Int =>  println(s"Its a Int : $n")
    case s : String => println(s"Its a String : $s")
    case default => println(s"Its something else : $default")
  }

  def doIt2(a:Int, b:Int) = (a,b) match {
    case(1,1) => println("1,1")
    case(1,_) => println("1,_")
    case(_,2) => println("_,2")
    case(_,_) => println("_,_")
  }

  def main(args:Array[String]): Unit = {
    val n=4
    doIt(n)
    doIt("abc")
    doIt(32.0)

    doIt1(n)
    doIt1("abc")
    doIt1(32.0)

    doIt2(1,4)

  }

}
