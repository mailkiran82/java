1 to 5

for( i <- 1 to 5){
  println(i)
}

val people = Map(("a",1),"b" -> 2 )

//destructuring
for((k,v) <- people) {
  println(k,v)
}

//nested loop
for(i <- 1 to 3){
  for(j <- 1 to 3) {
    print(i,j)
  }
}
for( i <- 1 to 3 ; j <- 1 to 3){
  println(i,j)
}

(1 to 3).foreach( x => (1 to 3).foreach( y => println(x,y) ))

//with gaurd
for((k,v) <- people if v == 2) {
  println(k,v)
}

val price =List (
  List(1,2,3),
  List(),
  List(5,6)
)

//add .5 to values  > 3
for {
  pr <- price
  p <- pr
  if p >= 3
} yield p+.5

price.flatMap( pr => pr.filter( p => p >=3).map( x => x+.5))