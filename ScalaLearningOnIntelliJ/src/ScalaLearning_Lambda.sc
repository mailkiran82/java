3

//give name to 3
val n = 3

//function object without name
(n:Int) => n*n
(n:Int,m:Int) => n*m

//val infers the type of square
val square = (n:Int) => n*n
println(square(2))
println(square.apply(2))

//IMP
//JAVA/ C++ - requires the lhs to have the type
//eg List<String>
//but in Scala the lhs is inferred from the rhs
// hence more details are provided on the rhs
//here on the rhs you can see both n&m have Int type
//but in JAVA lambda you would have this type info in rhs
val sum = (n:Int,m:Int) => n+m

//terminology : lambda = function literal = closure

val cube = (n:Int) => n*n*n

def doIt(n:Int, f:Int => Int): Int ={
  f.apply(n)
}

doIt(2,square)
doIt(3,cube)

//Function vs Methods
//Function
//function have Type
val sumIt = (n:Int,m:Int) => n+m
//Method
//methods used def (define)
//it always part of someting (object/worksheet)
//method have signature , they do not have type
object Test{
  def f(n:Int) = n+n
}


//use a method in a literal
//wrap the method in a lambda
//rhs return the output of funciton f
val f1 = (n:Int) => Test.f(n)
f1(2)
//shorter was to write above
// it says take the body Test.f and make is as body of my apply function
//similar to method reference using :: in Java/CPP
//rhs return the function reference
val f2 = (n:Int) => Test.f _
f1(2)

//partial functions
//terminology from mathematics
//domain - i/p
//target - o/p
//entire function - for any domain(i/p) you will get a target(o/p)
//partial functions looks like a map (key,value)
// the below does not have a default(_)
val p:PartialFunction[Int,String] = {
  case 1 => "one"
  case 2 => "two"
}

p(1)
p(3)