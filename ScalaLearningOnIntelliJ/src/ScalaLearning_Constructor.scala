// demonstrate chain of contructors
import java.time.LocalDateTime



class ScalaLearning_Constructor ( val amount:Double,
                               val currency:String,
                               val due:LocalDateTime) {

  // override the above default constructor
    //auxiliary consturctor
  def this(amount:Double, due:LocalDateTime) = this(amount,"USD",due) // calls the above primary constructor

  def this(amount:Double) = this (amount,LocalDateTime.now()) // calls the above aux constructor


  val settleDate = due.toLocalDate.plusDays(2)  //evaluated when its called/referenced

  //rollforward in case of holiday
  //since settleDate is immutable you can create a new object
  private lazy val processedAt = LocalDateTime.now()
  def rollForward() = {
    val retval = new ScalaLearning_Constructor(amount,currency,due.plusDays(1))
    retval.processedAt
    retval
  }

}

//object ScalaLearningAdvanced2 is a companion of the above class
//and it holds the static components

//put the instance methods and members in the class
//and put the static in the object with the same name

object ScalaLearning_Constructor{
  def main(args: Array[String]): Unit ={
    val c1 = new ScalaLearning_Constructor(100.0)
    println(c1.settleDate)

    val c2 = c1.rollForward()
    Thread.sleep(1000)
    println(c2.settleDate)
    println(c1.processedAt)
    println(c2.processedAt)
  }
}