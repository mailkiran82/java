val people = Map(("a",1),"b" -> 2 )
val people0 = Map("b"->4)

println(people)

val p = people("a")
val p1 = people("d")

val p2 = people.get("d")

val p3 = people.getOrElse("d","Not Fund")

val p4 = people.contains("d")

val p5 = people.isDefinedAt("d")

val people1 = people + ("c"->3,"d"->4)
println(people1)

val people2= people1 ++ people0
println(people2)

