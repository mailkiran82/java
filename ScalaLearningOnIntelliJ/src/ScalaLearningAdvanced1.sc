// if no parameters are passed, then () is optional in function call
def foo0() ={
  print(s"a")
}
foo0

//default arguments need () in function call
def foo1(a:Int=0, b:String ="abc",c:Int=10) ={
  print(s"a:$a, b:$b, c:$c")
}
foo1()
foo1(10)
foo1(b="xyz")

//variable argument list
def sum(nums: Int*) = {
  var total =0
  for (n <- nums){
    total += n
  }
  total
}
sum(1,2,3)

//imports and packages
import java.util.ArrayList

val l = new ArrayList[String]
l.add("Hi")
println(l)

// _ is used as * is scala
import java.util.concurrent._
import java.util.{Map, HashMap}
import java.util.{Date => UData}

def factorial(n: Int):Int = {
  if( n == 0) 1 else n * factorial(n-1)
   }
println(factorial(10))

val list = List.range(1,11)
println(list)

def factorial1(l:List[Int]):Int ={
  if(l.isEmpty) 1
  else l.head * factorial1(l.tail)
}
println(factorial1(list))