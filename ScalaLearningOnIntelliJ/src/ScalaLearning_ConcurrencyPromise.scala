import scala.concurrent._

//promise - seperate teh read (main) write(thread) operation
// client(main ) calls get to get the value.
// write once and read many. promise holds the value
//you can have the callback registered, if the value changes the callback is called
object ScalaLearning_ConcurrencyPromise {

  def main(args:Array[String]): Unit ={
    //println("hello")
    implicit val ex = ExecutionContext.global
    val f1 = Future { Thread.sleep(1000)
    50}

    val f2 = Future { Thread.sleep(1000)
      25}

    //PROMISE id WRITE ONLU
    //FUTURE is READ ONLY
    //you can have many FUTURES for a PROMISE
    val p1 = Promise[Int]
    val pf1 = p1.future

    val p2 = Promise[Int]
    val pf2 = p2.future

    Thread.sleep(2000)

    println(f1.value.get.get)
    println(f2.value.get.get)

    p1.success(f1.value.get.get)
    p2.success(f2.value.get.get)

    //r is also a future
    val r = for {
      a <- pf1
      b <- pf2
    } yield a+b

    Thread.sleep(2000)
    println("Value:"+r.value)
  }
}
