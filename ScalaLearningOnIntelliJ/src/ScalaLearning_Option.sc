val opt = Some(1)
val none = None

//type called Option[T]
// it a tiny generic container
opt.get

case class User( name:String, city:Option[String])

val u1 = User("a",Some("BLR"))
val u2 = User("a",None)

u1.city.get
//u2.city.get
u1.city.getOrElse("no city info")
u2.city.getOrElse("no city info")

def getCity(u:User) =  u.city match {
  case Some(c) =>  c
  case None => "no city info"
}

getCity(u1)
getCity(u2)

val users = List (
  User("a",Some("BRL")),
  User("b",None),
  User("c",Some("GGN")),
  User("d",None)
)

val cities = users.map( x => x.city)
val cities1 = users.flatMap( x => x.city)