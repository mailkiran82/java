
val s = "123"
try{
  val n = s.toInt
}
catch {
  case ex : NumberFormatException => println("NumberFormatException:"+ex.getMessage)
}

// function way in scala
//success case has a payload -value
//failure case also has a payload with exception
import scala.util.{Try,Success,Failure}
val v1 = Try { s.toInt}


def getValue(v:Try[Int]) = v1 match {
  case Success(n) => n
  case Failure(ex) => -1
}

getValue(v1)

val s1 = "321"

for {
  n <- Try {s.toInt}
  m <- Try {s1.toInt}
} yield n+m