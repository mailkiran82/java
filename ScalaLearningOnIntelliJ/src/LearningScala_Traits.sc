trait Vehicle {
  def launch
}

trait Boat extends Vehicle {
  override def launch = println("Boat")
}

trait AnotherBoat extends  Vehicle {
  override def launch = println("AnotherBoat")
}

trait Plane extends Vehicle {
  override def launch  = println("Plane")
}


//picks the launch implementation from right to left
class Seaplane extends Plane with Boat{

}

val s = new Seaplane
s.launch

//you can add another mock trait for testing without changing the existing
val s1 = new Seaplane with AnotherBoat
s1.launch