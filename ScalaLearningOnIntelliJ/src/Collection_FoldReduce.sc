val list = List(1,2,3)

//1st argument list 0 is starting point
//2nd argument list ia labmda
list.foldLeft(0)(
  (acc,nxt) => acc+nxt
)

list.foldLeft(1)(
  (acc,nxt) => acc*nxt
)

val list2 =List("a","b","c")
//it internally creates string builder for each element
//hence need to be careful for large list
//+is commutative here a+b = b+a even for string
list2.foldLeft("")(
  (a,b) => a+b
)

list2.foldRight("")(
  (a,b) => a+b
)


list.reduce( (acc,nxt) => acc+nxt)
list.reduce( _+_)


// traverses the entire list even after it found the item
//hence may not be optimized way to find
def contains[T](lst:List[T],item:T): Boolean ={
  lst.foldLeft(false)( (a,b) => a || b == item)
}

contains(list,2)
contains(list,4)

contains(list2,"b")
contains(list2,"d")

def reverse[T](lst:List[T]): List[T] = {
  lst.foldLeft(List[T]())( (a,b) => b :: a)
}

println(reverse(list2))
